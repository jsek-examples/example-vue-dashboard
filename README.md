# example-vue-dashboard

![](./screenshot.png)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

<!-- https://coreui.io/vue/demo/#/widgets -->