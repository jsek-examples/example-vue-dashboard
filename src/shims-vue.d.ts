declare module '*.vue' {
    import Vue from 'vue'

    interface LoaderArgs {
        container: Vue | Element | Vue[] | Element[]
    }

    interface LoadingPlugin {
        show(args: LoaderArgs): {
            hide(): void
        }
    }

    module "vue/types/vue" {
        interface Vue {
            readonly $loading: LoadingPlugin;
        }
    }

    export default Vue
}

declare module 'vue-loading-overlay' {
    import { PluginObject } from 'vue'
    const Loading: PluginObject<any>
    export default Loading
}

declare module 'vue-highcharts' {
    import { PluginObject } from 'vue'
    const VueHighcharts: PluginObject<any>
    export default VueHighcharts
}