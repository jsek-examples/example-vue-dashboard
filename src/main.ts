import Vue from 'vue'
import App from './App.vue'
import store from './store'

import Loading from 'vue-loading-overlay';
import VueHighcharts from 'vue-highcharts';
import * as Highcharts from "highcharts";
import * as G from "gsap";

import 'vue-loading-overlay/dist/vue-loading.css'

Vue.use(Loading);
Vue.use(VueHighcharts);

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
